#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//adding mem
unsigned char mem[2 << 24 - 1];

int log2(int n) {
    int r=0;
    while (n>>=1) r++;
        return r;
}

int ones(int n) {
    return ((1<<n) - 1);
}

int bottomN(int sample, int n) {
    int comparator = ones(n);
    return sample&comparator;
}

int main(int argc, char ** argv) {
    //reading in all of our command line values
    FILE* trace = fopen(argv[1], "r");
    int cacheSize = atoi(argv[2]) * 1024; //bc KB
    int assoc = atoi(argv[3]);
    int blkSize = atoi(argv[4]);

    //doing our basic cache math
    int numFrames = cacheSize / blkSize;
    int sets = numFrames / assoc;

    //address math
    int offsetBits = log2(blkSize);
    int indexBits = log2(sets);
    int tagBits = 24 - offsetBits - indexBits;

    int cache[sets][assoc];
    for(int i = 0; i < sets; i++) {
        for(int j = 0; j < assoc; j++) {
            cache[i][j] = -1;
        }
    }
    while(true) {
        /*if(strcmp(opStr, EOF) == 0) {
            break;
        }*/
        char opStr[24];
        if (fscanf(trace, "%s\n", opStr) == EOF) {                  //check if end of file
            break;
        }
        int address = -1;
        int accessSize = -1;
        int loadedVal = -1;
        if(strcmp(opStr, "store") == 0) {
            fscanf(trace, "0x%x\n", &address);
            fscanf(trace, "%d\n", &accessSize);
            int tempAddress = address;
            int block = bottomN(tempAddress, offsetBits);
            tempAddress = tempAddress>>offsetBits;
            int set = bottomN(tempAddress, indexBits);
            int tag = tempAddress>>indexBits;
            int isInIndex = -1;
            int isHit = -1;
            for(int i = 0; i < assoc; i++) {
                if(cache[set][i] == tag) {
                    isInIndex = i;
                    for (int j = isInIndex; j > 0; j--){
                        cache[set][j] = cache[set][j-1];
                    }
                    cache[set][0] = tag;
                    printf("%s 0x%x hit\n", opStr, address);
                    isHit = 1;
                }
            }
            if(isHit != 1) {
                printf("%s 0x%x miss\n", opStr, address);
            }
            for(int j = 0; j < accessSize; j++) {
                fscanf(trace, "%02hhx", &mem[address + j]);
            }
        }
        if(strcmp(opStr, "load") == 0) {
            fscanf(trace, "0x%x\n", &address);
            fscanf(trace, "%d\n", &accessSize);
            int tempAddress = address;
            int block = bottomN(tempAddress, offsetBits);
            tempAddress = tempAddress>>offsetBits;
            int set = bottomN(tempAddress, indexBits);
            int tag = tempAddress>>indexBits;
            int isInIndex = -1;
            int isHit = -1;
            for(int i = 0; i < assoc; i++) {
                if(cache[set][i] == tag) {
                    isInIndex = i;
                    for (int j = isInIndex; j > 0; j--){
                        cache[set][j] = cache[set][j-1];
                    }
                    cache[set][0] = tag;
                    printf("%s 0x%x hit ", opStr, address);
                    for (int j = 0; j < accessSize; j++) {
                        printf("%02hhx", mem[address + j]);
                    }
                    isHit = 1;
                    printf("\n");
                }
            }
            if (isHit != 1) {
                for (int j = (assoc - 1); j > 0; j--){
                    cache[set][j] = cache[set][j-1];
                }
                cache[set][0] = tag;
                printf("%s 0x%x miss ", opStr, address);
                for (int j = 0; j < accessSize; j++) {
                    printf("%02hhx", mem[address + j]);
                }
                printf("\n");
            }
        }
    }
    fclose(trace);
    return EXIT_SUCCESS;
}