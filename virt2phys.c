#include <stdio.h>
#include <stdlib.h>
#include <cstring>

int log2(int n) {
    int r=0;
    while (n>>=1) r++;
        return r;
}

int ones(int n) {
    return ((1<<n) - 1);
}

int bottomN(int sample, int n) {
    int comparator = ones(n);
    return sample&comparator;
}

int main(int argc, char ** argv) {
    FILE* pageInfo = fopen(argv[1], "r");       //opens the file for reading
    char addressStr[24];
    fscanf(pageInfo, "%s", addressStr);   //takes first string, saves as #of address bits
    int addressBits = atoi(addressStr);
    char sizeStr[24];
    fscanf(pageInfo, "%s", sizeStr);  //takes second hex val, saves as page size
    int pageSize = atoi(sizeStr);
    int shiftBits = log2(pageSize);  //finds the number of bits required for page size in addresses
    int searchAddress = (int)strtol(argv[2], NULL, 16); //saves our address in question
    int offset = bottomN(searchAddress, shiftBits);
    searchAddress = searchAddress>>shiftBits;   //shifting our address so it isolates just the page bits
    int virtPage = 0;   //allows for iteration through our pages
    int physPage = -1;  //standard value for physical page is invalid
    char pageStr[24];
    while (true) {
        fscanf(pageInfo, "%s\n", pageStr);  //replaces physpage val if valid, rewrites to -1 if invalid
        physPage = atoi(pageStr);
        if(virtPage == searchAddress) { //break if we hit our search address or we run out of virt pages
            break;
        }
        virtPage = virtPage + 1; // continue iterating through virt pages
    }
    if(physPage == -1) {    //returns dictated by assignment
        printf("PAGEFAULT\n");
    }
    else {
        physPage = physPage << shiftBits;
        physPage = physPage | offset;
        printf("%x\n", physPage);
    }
    //printf("%x\n", offset);
    fclose(pageInfo);
    return EXIT_SUCCESS;
}